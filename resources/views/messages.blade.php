@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="container mt-5 mb-5">
            <div class="d-flex justify-content-center row">
                <div class="d-flex flex-column col-md-8">
                    <div class="bg-white p-2 px-4">
                        <div class="mt-4 mb-4 parent">
                            <div contenteditable="true" class="border border-dark w-100 mr-3 custom-textarea"></div>
                            <div class="d-flex align-items-center">
                                <button class="btn btn-primary mt-2" type="button">
                                    Comment
                                    <div class="spinner-border" style="width: 1rem;height: 1rem; display: none"
                                         role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </button>
                                <div class="mt-2 ml-4 d-flex align-items-center">
                                    <a href="#" class="upload_image">Загрузить картинку</a>
                                    <input type="file" class="input-upload" name="image" accept="image/*" hidden>
                                    <div class="spinner-border" role="status" style="display: none">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-2">
                            <div class="d-flex flex-row align-items-center">
                                <h5 class="mr-2">Corey oates</h5><span class="dot mb-1"></span><span class="mb-1 ml-2">4 hours ago</span>
                            </div>
                            <div>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </div>
                            <div class="reply-section">
                                <a href="#" class="mt-1">Reply</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Ошибка!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Ок</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            var body = $('body')

            function showModal(title, body) {
                var myModal = $('#myModal')
                myModal.find('#title').text(title)
                myModal.find('#modal-body').html(body)
                myModal.modal('show')
            }

            body.on('click', '.upload_image', function (e) {
                e.preventDefault()
                $(this).next().click()
            })

            body.on('change', '.input-upload', function (e) {
                var input = $(this)
                input.prev().hide()
                input.next().show()
                var formData = new FormData()
                formData.set('image', this.files[0])
                formData.set('_token', "{{csrf_token()}}")

                $.ajax({
                    url: "{{route('messages.upload')}}",
                    type: "POST",
                    processData: false,
                    contentType: false,
                    dataType: 'JSON',
                    data: formData,
                    success: function (data) {
                        if (!data.success) {
                            showModal('Ошибка!', '<div class="alert alert-danger" role="alert">Что-то пошло не так!</div>')
                        }
                        input.parents('.parent')
                            .find('.custom-textarea')
                            .append(`<img src="https://cdn.somon.tj/media/cache3/9c/4f/9c4f336e828fa41a82552d2fdcb52cc7.jpg" alt="image" >`)
                    },
                    error: function (data) {
                        if (data.status === 400) {
                            var errors = data.responseJSON.errors,
                                html = ''
                            errors['image'].forEach(function (item) {
                                html += `<div class="alert alert-danger" role="alert">${item}</div>`
                            })
                            showModal('Ошибка!', html)
                        }
                    },
                    complete: function () {
                        input.prev().show()
                        input.next().hide()
                    }
                })
            })
        });

    </script>
@endsection
