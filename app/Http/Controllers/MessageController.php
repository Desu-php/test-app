<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    //
    public function index()
    {
        return view('messages');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|string|max:1000',
            'reply' => 'nullable|exists:messages,id'
        ]);

        if ($validator->fails()){
            return Response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()
            ], 400);
        }

        Message::craete([
            'message' => $request->message,
            'user_id' => Auth::id(),
            'reply_id' => !empty($request->reply)?$request->reply:0
        ]);

        return  Response()->json([
            'success' => true,
            'message' => 'Вы успешно отправили сообщение'
        ]);
    }

    public function uploadImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|mimes:png,jpg,jpeg,png,webp|dimensions:min_width=100,min_height=100',
        ]);

        if ($validator->fails()){
            return Response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()
            ], 400);
        }


    }
}
